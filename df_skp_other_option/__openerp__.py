# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Kab Belitung Timur
#    Copyright (c) 2015 <http://www.->
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################
{
    "name": "Konfigurasi Proses Dan Setting Aplikasi SKP Admin Input",
    "version": "1.0",
    "author": "Darmawan Fatriananda",
    "category": "Project/SKP",
    "description": "Konfigurasi Proses Dan Setting Aplikasi SKP Admin Input",
    "website" : "http://-",
       "license" : "GPL-3",
    "depends": ['df_partner_employee'],
    'data': ["other_option_configuration_view.xml",
           
             ],
    'installable': True,
    'active': False,
}
