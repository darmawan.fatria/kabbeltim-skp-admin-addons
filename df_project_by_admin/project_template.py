# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Kab Belitung Timur
#    Copyright (c) 2015 <http://www.->
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp
from openerp import SUPERUSER_ID
import time
from mx import DateTime
from openerp import netsvc

# ====================== Project Template ================================= #
class project_template(osv.Model):
    _name = 'project.template'
    _description='Template Project By admin'
    _columns = {
        'project_id': fields.many2one('project.project', 'Target', ondelete='set null', ),
        'company_name'         : fields.char('SKPD',size=64, required=True),
        'company_code'          : fields.char('Kode OPD',size=18, required=False),
        'target_period_year'    : fields.char('Periode Tahun',size=4, required=True),

        'name'                       : fields.char('Nama Kegiatan',size=500, required=True),
        'code'                        : fields.char('Kode Kegiatan',size=25,required=False),

        'nip'                           : fields.char('NIP',size=20, required=True),
        'employee_name'        : fields.char('Nama Pegawai',size=64, required=False),
        'target_type_id': fields.selection([          ('dpa_opd_biro', 'Kegiatan APBD - DPA'),
                                                      ('dipa_apbn', 'Kegiatan APBN - DIPA'),  
                                                      ('sotk', 'Kegiatan Tupoksi (Non DPA)'), 
                                                      ('lain_lain', 'Kegiatan Target Pendapatan Daerah'), 
                                                      ],
                                                      'Jenis Kegiatan', required=True, 
                                                     ),
         'target_category_id': fields.selection([     ('program', 'Program'), 
                                                      ('kegiatan', 'Kegiatan'), 
                                                      ],
                                                      'Kategori',
                                                     ),
        'target_jumlah_kuantitas_output'     : fields.float('Kuantitas Output',required=True,digits_compute=dp.get_precision('no_digit')),
        'target_satuan_kuantitas_output'         : fields.char('Satuan Kuantitas Output',size=64, required=True),
        'target_kualitas'     : fields.float('Kualitas',required=True,digits_compute=dp.get_precision('no_digit')),
        'target_waktu'     : fields.integer('Waktu',required=True,digits_compute=dp.get_precision('no_digit')),
        'target_biaya'     : fields.float('Biaya',),
        'target_angka_kredit'                       : fields.float('Angka Kredit' ,digits_compute=dp.get_precision('angka_kredit')),

        'target_jumlah_kuantitas_output_01'        : fields.float('Kuantitas Output Januari',required=False,digits_compute=dp.get_precision('no_digit')),
        'target_satuan_kuantitas_output_01'        : fields.char('Satuan Kuantitas Output Januari',size=64, required=False),
        'target_kualitas_01'                               : fields.float('Kualitas Januari',required=False,digits_compute=dp.get_precision('no_digit')),
        'target_waktu_01'                                  : fields.integer('Waktu Januari',required=False,digits_compute=dp.get_precision('no_digit')),
        'target_biaya_01'                                  : fields.float('Biaya Januari',),
        'target_angka_kredit_01'                       : fields.float('Angka Kredit Januari' ,digits_compute=dp.get_precision('angka_kredit')),

        'target_jumlah_kuantitas_output_02'        : fields.float('Kuantitas Output Februari',required=False,digits_compute=dp.get_precision('no_digit')),
        'target_satuan_kuantitas_output_02'        : fields.char('Satuan Kuantitas Output Februari',size=64, required=False),
        'target_kualitas_02'                               : fields.float('Kualitas Februari',required=False,digits_compute=dp.get_precision('no_digit')),
        'target_waktu_02'                                  : fields.integer('Waktu Februari',required=False,digits_compute=dp.get_precision('no_digit')),
        'target_biaya_02'                                  : fields.float('Biaya Februari',),
        'target_angka_kredit_02'                       : fields.float('Angka Kredit Februari' ,digits_compute=dp.get_precision('angka_kredit')),

        'target_jumlah_kuantitas_output_03'        : fields.float('Kuantitas Output Maret',required=False,digits_compute=dp.get_precision('no_digit')),
        'target_satuan_kuantitas_output_03'        : fields.char('Satuan Kuantitas Output Maret',size=64, required=False),
        'target_kualitas_03'                               : fields.float('Kualitas Maret',required=False,digits_compute=dp.get_precision('no_digit')),
        'target_waktu_03'                                  : fields.integer('Waktu Maret',required=False,digits_compute=dp.get_precision('no_digit')),
        'target_biaya_03'                                  : fields.float('Biaya Maret',),
        'target_angka_kredit_03'                       : fields.float('Angka Kredit Maret' ,digits_compute=dp.get_precision('angka_kredit')),

        'target_jumlah_kuantitas_output_04'        : fields.float('Kuantitas Output April',required=False,digits_compute=dp.get_precision('no_digit')),
        'target_satuan_kuantitas_output_04'        : fields.char('Satuan Kuantitas Output April',size=64, required=False),
        'target_kualitas_04'                               : fields.float('Kualitas April',required=False,digits_compute=dp.get_precision('no_digit')),
        'target_waktu_04'                                  : fields.integer('Waktu April',required=False,digits_compute=dp.get_precision('no_digit')),
        'target_biaya_04'                                  : fields.float('Biaya April',),
        'target_angka_kredit_04'                       : fields.float('Angka Kredit April' ,digits_compute=dp.get_precision('angka_kredit')),

        'target_jumlah_kuantitas_output_05'        : fields.float('Kuantitas Output Mei',required=False,digits_compute=dp.get_precision('no_digit')),
        'target_satuan_kuantitas_output_05'        : fields.char('Satuan Kuantitas Output Mei',size=64, required=False),
        'target_kualitas_05'                               : fields.float('Kualitas Mei',required=False,digits_compute=dp.get_precision('no_digit')),
        'target_waktu_05'                                  : fields.integer('Waktu Mei',required=False,digits_compute=dp.get_precision('no_digit')),
        'target_biaya_05'                                  : fields.float('Biaya Mei',),
        'target_angka_kredit_05'                       : fields.float('Angka Kredit Mei' ,digits_compute=dp.get_precision('angka_kredit')),

        'target_jumlah_kuantitas_output_06'        : fields.float('Kuantitas Output Juni',required=False,digits_compute=dp.get_precision('no_digit')),
        'target_satuan_kuantitas_output_06'        : fields.char('Satuan Kuantitas Output Juni',size=64, required=False),
        'target_kualitas_06'                               : fields.float('Kualitas Juni',required=False,digits_compute=dp.get_precision('no_digit')),
        'target_waktu_06'                                  : fields.integer('Waktu Juni',required=False,digits_compute=dp.get_precision('no_digit')),
        'target_biaya_06'                                  : fields.float('Biaya Juni',),
        'target_angka_kredit_06'                       : fields.float('Angka Kredit Juni' ,digits_compute=dp.get_precision('angka_kredit')),

        'target_jumlah_kuantitas_output_07'        : fields.float('Kuantitas Output Juli',required=False,digits_compute=dp.get_precision('no_digit')),
        'target_satuan_kuantitas_output_07'        : fields.char('Satuan Kuantitas Output Juli',size=64, required=False),
        'target_kualitas_07'                               : fields.float('Kualitas Juli',required=False,digits_compute=dp.get_precision('no_digit')),
        'target_waktu_07'                                  : fields.integer('Waktu Juli',required=False,digits_compute=dp.get_precision('no_digit')),
        'target_biaya_07'                                  : fields.float('Biaya Juli',),
        'target_angka_kredit_07'                       : fields.float('Angka Kredit Juli' ,digits_compute=dp.get_precision('angka_kredit')),

        'target_jumlah_kuantitas_output_08'        : fields.float('Kuantitas Output Agustus',required=False,digits_compute=dp.get_precision('no_digit')),
        'target_satuan_kuantitas_output_08'        : fields.char('Satuan Kuantitas Output Agustus',size=64, required=False),
        'target_kualitas_08'                               : fields.float('Kualitas Agustus' ,required=False,digits_compute=dp.get_precision('no_digit')),
        'target_waktu_08'                                  : fields.integer('Waktu Agustus',required=False,digits_compute=dp.get_precision('no_digit')),
        'target_biaya_08'                                  : fields.float('Biaya Agustus',),
        'target_angka_kredit_08'                       : fields.float('Angka Kredit' ,digits_compute=dp.get_precision('angka_kredit')),

        'target_jumlah_kuantitas_output_09'        : fields.float('Kuantitas Output September',required=False,digits_compute=dp.get_precision('no_digit')),
        'target_satuan_kuantitas_output_09'        : fields.char('Satuan Kuantitas Output September',size=64, required=False),
        'target_kualitas_09'                               : fields.float('Kualitas September',required=False,digits_compute=dp.get_precision('no_digit')),
        'target_waktu_09'                                  : fields.integer('Waktu September',required=False,digits_compute=dp.get_precision('no_digit')),
        'target_biaya_09'                                  : fields.float('Biaya September',),
        'target_angka_kredit_09'                       : fields.float('Angka Kredit September' ,digits_compute=dp.get_precision('angka_kredit')),

        'target_jumlah_kuantitas_output_10'        : fields.float('Kuantitas Output Oktober',required=False,digits_compute=dp.get_precision('no_digit')),
        'target_satuan_kuantitas_output_10'        : fields.char('Satuan Kuantitas Output Oktober',size=64, required=False),
        'target_kualitas_10'                               : fields.float('Kualitas Oktober',required=False,digits_compute=dp.get_precision('no_digit')),
        'target_waktu_10'                                  : fields.integer('Waktu Oktober',required=False,digits_compute=dp.get_precision('no_digit')),
        'target_biaya_10'                                  : fields.float('Biaya Oktober',),
        'target_angka_kredit_10'                       : fields.float('Angka Kredit Oktober' ,digits_compute=dp.get_precision('angka_kredit')),

        'target_jumlah_kuantitas_output_11'        : fields.float('Kuantitas Output November',required=False,digits_compute=dp.get_precision('no_digit')),
        'target_satuan_kuantitas_output_11'        : fields.char('Satuan Kuantitas Output November',size=64, required=False),
        'target_kualitas_11'                               : fields.float('Kualitas November',required=False,digits_compute=dp.get_precision('no_digit')),
        'target_waktu_11'                                  : fields.integer('Waktu November',required=False,digits_compute=dp.get_precision('no_digit')),
        'target_biaya_11'                                  : fields.float('Biaya November',),
        'target_angka_kredit_11'                       : fields.float('Angka Kredit November' ,digits_compute=dp.get_precision('angka_kredit')),

        'target_jumlah_kuantitas_output_12'        : fields.float('Kuantitas Output Desember',required=False,digits_compute=dp.get_precision('no_digit')),
        'target_satuan_kuantitas_output_12'        : fields.char('Satuan Kuantitas Output Desember',size=64, required=False),
        'target_kualitas_12'                               : fields.float('Kualitas Desember',required=False,digits_compute=dp.get_precision('no_digit')),
        'target_waktu_12'                                  : fields.integer('Waktu Desember',required=False,digits_compute=dp.get_precision('no_digit')),
        'target_biaya_12'                                  : fields.float('Biaya Desember',),
        'target_angka_kredit_12'                       : fields.float('Angka Kredit Desember' ,digits_compute=dp.get_precision('angka_kredit')),

        'active_01' :fields.boolean('Januari'),
        'active_02' :fields.boolean('Februari'),
        'active_03' :fields.boolean('Maret'),
        'active_04' :fields.boolean('April'),
        'active_05':fields.boolean('Mei'),
        'active_06' :fields.boolean('Juni'),
        'active_07' :fields.boolean('Juli'),
        'active_08' :fields.boolean('Agustus'),
        'active_09' :fields.boolean('September'),
        'active_10' :fields.boolean('Oktober'),
        'active_11' :fields.boolean('November'),
         'active_12' :fields.boolean('Desember'),

         'state': fields.selection([     ('new', 'Baru'),  ('valid', 'Valid'),  ('invalid', 'Invalid'), ('done', 'Selesai'),   ],
                                                      'Status',readonly=True
                                                     ),
          'error_count'     : fields.integer('Jumlah Revisi',readonly=True),
          'notes'     : fields.text('Catatan'),

          'user_id_admin_opd': fields.many2one('res.users', 'Petugas Admin Dari SKPD', 
            help='Staff admin SKPD'),
          'user_id': fields.many2one('res.users', 'Pegawai Yang Dinilai',),
          'user_id_atasan': fields.many2one('res.users', 'Atasan Langsung', ),
          'user_id_banding': fields.many2one('res.users', 'Atasan Banding', ),
          
    }
    _defaults = {
        'state':'new',
        'target_period_year':lambda *args: time.strftime('%Y'),
        
    }
    def action_target_evaluated(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        wf_service = netsvc.LocalService("workflow")
        for template_obj in self.browse(cr, uid, ids, context=context):
            wf_service.trg_validate(template_obj.user_id.id, 'project.project', template_obj.project_id.id, 'action_propose', cr)
            wf_service.trg_validate(template_obj.user_id_atasan.id, 'project.project', template_obj.project_id.id, 'action_evaluated', cr)
            self.write(cr,uid,[template_obj.id],{'state':'done',},context=None)
                


    def create_target_from_template(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        target_pool = self.pool.get('project.project')
        wf_service = netsvc.LocalService("workflow")
        

        target =  {}
        now=DateTime.today();
        january=DateTime.Date(now.year,1,1)
        january_date =  DateTime.strptime(january.strftime('%Y-%m-%d'),'%Y-%m-%d')

        for template_obj in self.browse(cr, uid, ids, context=context):
                error_count = 0
                notes='-'
                company_obj , company_err_msg= self.get_company_by_name(cr,uid,template_obj.company_name)
                employee_obj, employee_err_msg = self.get_employee_by_nip(cr,uid,template_obj.nip)

                if not employee_obj:
                    error_count+=1
                    notes= notes+' '+employee_err_msg+', '
                else :
                    company_name = company_obj and company_obj.name or '-'
                    company_code = company_obj and company_obj.code or '-'
                    company_id = company_obj and company_obj.id or None
                    user_id_bkd = company_obj and company_obj.user_id_bkd and company_obj.user_id_bkd
                if not company_obj:
                    error_count+=1
                    notes= notes+' '+company_err_msg
                else :
                    employee_name  = employee_obj and employee_obj.name or '-'
                    employee_id  = employee_obj and employee_obj.id or '-'
                    user_id  = employee_obj and employee_obj.user_id or '-'
                    job_type = employee_obj and employee_obj.job_type
                    user_id_atasan = employee_obj and employee_obj.user_id_atasan and employee_obj.user_id_atasan.user_id
                    user_id_banding =  employee_obj and employee_obj.user_id_banding and employee_obj.user_id_banding.user_id
                if error_count > 0 :
                    self.write(cr,uid,[template_obj.id],{   'state':'invalid','error_count' :error_count, 'notes':notes,
                                                                },context=None)
                    continue;

                satuan_hitung_id,satuan_hitung_msg = self.get_satuan_satuan_hitung(cr,uid,template_obj.target_satuan_kuantitas_output)

                target.update({
                               'name'                :template_obj.name,
                               'code'                 : template_obj.code,
                               'code_opd'          :company_name ,
                               'state'                : 'draft' ,
                               'lama_kegiatan'   : template_obj.target_waktu,
                               'satuan_lama_kegiatan'                :'bulan',
                               'target_type_id'                 :template_obj.target_type_id ,
                               'target_category_id'                : template_obj.target_category_id,
                               'target_period_year'   :template_obj.target_period_year ,
                               'target_jumlah_kuantitas_output'                :template_obj.target_jumlah_kuantitas_output,
                               'target_satuan_kuantitas_output'                 : satuan_hitung_id,
                               'target_angka_kredit'          : template_obj.target_angka_kredit,
                               'target_kualitas'                : template_obj.target_kualitas,
                               'target_waktu'   :template_obj.target_waktu ,
                               'target_satuan_waktu'   :'bulan' ,
                               'target_biaya'                :template_obj.target_biaya,
                               'user_id'                 : user_id.id,
                               'user_id_atasan'          :  user_id_atasan.id,
                               'user_id_banding'                : user_id_banding.id,
                               'user_id_bkd'   : user_id_bkd.id,
                               'active'                 : True,
                               'job_type'          : job_type ,
                               'company_id'                : company_id,
                               'currency_id'   : 13,
                               'status_target_bulanan':'sudah',
                                'date_start':january_date or False,
                                'user_id_admin_opd' : company_obj.user_id_admin_opd.id,
                                'error_count' :error_count,
                                'notes':notes,
                     })
                new_target_id = target_pool.create(cr,user_id.id,target,context=None)

                wf_service.trg_validate(user_id.id, 'project.project', new_target_id, 'action_new', cr)
                self.write(cr,uid,[template_obj.id],{'project_id':new_target_id,'employee_name':employee_name,
                                                                'user_id':user_id.id,
                                                                'user_id_atasan':user_id_atasan.id,
                                                                'user_id_banding':user_id_banding.id,
                                                                         'state':'valid',
                                                                                'user_id_admin_opd' : company_obj.user_id_admin_opd.id,
                                                                },context=None)
                self.generate_project_monthly_template(cr, uid, template_obj,new_target_id,company_id,user_id.id,user_id_atasan.id,user_id_banding.id,user_id_bkd.id,company_obj.user_id_admin_opd.id,context=None)

    def generate_project_monthly_template(self, cr, uid, template_obj,new_target_id,company_id,user_id,user_id_atasan,user_id_banding,user_id_bkd,user_id_admin_opd,context=None):

        task_pool = self.pool.get('project.skp')
        task =  {}

        task.update({
                           'project_id':new_target_id,
                           'user_id':user_id,
                           'company_id':company_id,
                           'name': template_obj.name,
                           'code': template_obj.code,
                           # 'code_opd':target_obj.code_opd,
                           #'code_program':target_obj.code_program,
                           #'code_kegiatan':target_obj.code_kegiatan,
                           'target_type_id':template_obj.target_type_id,
                           'target_category_id':template_obj.target_category_id,
                           'target_period_year': template_obj.target_period_year,
                            'user_id_atasan': user_id_atasan or False,
                            'user_id_banding':user_id_banding or False,
                            'user_id_bkd':user_id_bkd or False,
                            'notes':'-',
                            'currency_id':13,
                            'user_id_admin_opd' : user_id_admin_opd,

                            'state':'draft',
                            'active':True,
                           })
        if template_obj.active_01:
            satuan_hitung_id,satuan_hitung_msg = self.get_satuan_satuan_hitung(cr,uid,template_obj.target_satuan_kuantitas_output_01)
            task.update({
                               'target_period_month':'01',
                               'target_jumlah_kuantitas_output'     : template_obj.target_jumlah_kuantitas_output_01,
                               'target_satuan_kuantitas_output'     : satuan_hitung_id,
                               'target_waktu'     : template_obj.target_waktu_01,
                               'target_kualitas'     : template_obj.target_kualitas_01,
                               'target_satuan_waktu'     : 'hari',
                               'target_angka_kredit'     : template_obj.target_angka_kredit_01,
                               'target_biaya'     : template_obj.target_biaya_01,
                     })
            task_id = task_pool.create(cr, user_id, task,context)

        if template_obj.active_02:
            satuan_hitung_id,satuan_hitung_msg = self.get_satuan_satuan_hitung(cr,uid,template_obj.target_satuan_kuantitas_output_02)
            task.update({
                               'target_period_month':'02',
                               'target_jumlah_kuantitas_output'     : template_obj.target_jumlah_kuantitas_output_02,
                               'target_satuan_kuantitas_output'     : satuan_hitung_id,
                               'target_waktu'     : template_obj.target_waktu_02,
                               'target_kualitas'     : template_obj.target_kualitas_02,
                               'target_satuan_waktu'     : 'hari',
                               'target_angka_kredit'     : template_obj.target_angka_kredit_02,
                               'target_biaya'     : template_obj.target_biaya_02,
                     })
            task_id = task_pool.create(cr, user_id, task,context)

        if template_obj.active_03:
            satuan_hitung_id,satuan_hitung_msg = self.get_satuan_satuan_hitung(cr,uid,template_obj.target_satuan_kuantitas_output_03)
            task.update({
                               'target_period_month':'03',
                               'target_jumlah_kuantitas_output'     : template_obj.target_jumlah_kuantitas_output_03,
                               'target_satuan_kuantitas_output'     : satuan_hitung_id,
                               'target_waktu'     : template_obj.target_waktu_03,
                               'target_kualitas'     : template_obj.target_kualitas_03,
                               'target_satuan_waktu'     : 'hari',
                               'target_angka_kredit'     : template_obj.target_angka_kredit_03,
                               'target_biaya'     : template_obj.target_biaya_03,
                     })
            task_id = task_pool.create(cr, user_id, task,context)

        if template_obj.active_04:
            satuan_hitung_id,satuan_hitung_msg = self.get_satuan_satuan_hitung(cr,uid,template_obj.target_satuan_kuantitas_output_04)
            task.update({
                               'target_period_month':'04',
                               'target_jumlah_kuantitas_output'     : template_obj.target_jumlah_kuantitas_output_04,
                               'target_satuan_kuantitas_output'     : satuan_hitung_id,
                               'target_waktu'     : template_obj.target_waktu_04,
                               'target_kualitas'     : template_obj.target_kualitas_04,
                               'target_satuan_waktu'     : 'hari',
                               'target_angka_kredit'     : template_obj.target_angka_kredit_04,
                               'target_biaya'     : template_obj.target_biaya_04,
                     })
            task_id = task_pool.create(cr, user_id, task,context)

        if template_obj.active_05:
            satuan_hitung_id,satuan_hitung_msg = self.get_satuan_satuan_hitung(cr,uid,template_obj.target_satuan_kuantitas_output_05)
            task.update({
                               'target_period_month':'05',
                               'target_jumlah_kuantitas_output'     : template_obj.target_jumlah_kuantitas_output_05,
                               'target_satuan_kuantitas_output'     : satuan_hitung_id,
                               'target_waktu'     : template_obj.target_waktu_05,
                               'target_kualitas'     : template_obj.target_kualitas_05,
                               'target_satuan_waktu'     : 'hari',
                               'target_angka_kredit'     : template_obj.target_angka_kredit_05,
                               'target_biaya'     : template_obj.target_biaya_05,
                     })
            task_id = task_pool.create(cr, user_id, task,context)

        if template_obj.active_06:
            satuan_hitung_id,satuan_hitung_msg = self.get_satuan_satuan_hitung(cr,uid,template_obj.target_satuan_kuantitas_output_06)
            task.update({
                               'target_period_month':'06',
                               'target_jumlah_kuantitas_output'     : template_obj.target_jumlah_kuantitas_output_06,
                               'target_satuan_kuantitas_output'     : satuan_hitung_id,
                               'target_waktu'     : template_obj.target_waktu_06,
                               'target_kualitas'     : template_obj.target_kualitas_06,
                               'target_satuan_waktu'     : 'hari',
                               'target_angka_kredit'     : template_obj.target_angka_kredit_06,
                               'target_biaya'     : template_obj.target_biaya_06,
                     })
            task_id = task_pool.create(cr, user_id, task,context)

        if template_obj.active_07:
            satuan_hitung_id,satuan_hitung_msg = self.get_satuan_satuan_hitung(cr,uid,template_obj.target_satuan_kuantitas_output_07)
            task.update({
                               'target_period_month':'07',
                               'target_jumlah_kuantitas_output'     : template_obj.target_jumlah_kuantitas_output_07,
                               'target_satuan_kuantitas_output'     : satuan_hitung_id,
                               'target_waktu'     : template_obj.target_waktu_07,
                               'target_kualitas'     : template_obj.target_kualitas_07,
                               'target_satuan_waktu'     : 'hari',
                               'target_angka_kredit'     : template_obj.target_angka_kredit_07,
                               'target_biaya'     : template_obj.target_biaya_07,
                     })
            task_id = task_pool.create(cr, user_id, task,context)

        if template_obj.active_08:
            satuan_hitung_id,satuan_hitung_msg = self.get_satuan_satuan_hitung(cr,uid,template_obj.target_satuan_kuantitas_output_08)
            task.update({
                               'target_period_month':'08',
                               'target_jumlah_kuantitas_output'     : template_obj.target_jumlah_kuantitas_output_08,
                               'target_satuan_kuantitas_output'     : satuan_hitung_id,
                               'target_waktu'     : template_obj.target_waktu_08,
                               'target_kualitas'     : template_obj.target_kualitas_08,
                               'target_satuan_waktu'     : 'hari',
                               'target_angka_kredit'     : template_obj.target_angka_kredit_08,
                               'target_biaya'     : template_obj.target_biaya_08,
                     })
            task_id = task_pool.create(cr, user_id, task,context)

        if template_obj.active_09:
            satuan_hitung_id,satuan_hitung_msg = self.get_satuan_satuan_hitung(cr,uid,template_obj.target_satuan_kuantitas_output_09)
            task.update({
                               'target_period_month':'09',
                               'target_jumlah_kuantitas_output'     : template_obj.target_jumlah_kuantitas_output_09,
                               'target_satuan_kuantitas_output'     : satuan_hitung_id,
                               'target_waktu'     : template_obj.target_waktu_09,
                               'target_kualitas'     : template_obj.target_kualitas_09,
                               'target_satuan_waktu'     : 'hari',
                               'target_angka_kredit'     : template_obj.target_angka_kredit_09,
                               'target_biaya'     : template_obj.target_biaya_09,
                     })
            task_id = task_pool.create(cr, user_id, task,context)

        if template_obj.active_10:
            satuan_hitung_id,satuan_hitung_msg = self.get_satuan_satuan_hitung(cr,uid,template_obj.target_satuan_kuantitas_output_10)
            task.update({
                               'target_period_month':'10',
                               'target_jumlah_kuantitas_output'     : template_obj.target_jumlah_kuantitas_output_10,
                               'target_satuan_kuantitas_output'     : satuan_hitung_id,
                               'target_waktu'     : template_obj.target_waktu_10,
                               'target_kualitas'     : template_obj.target_kualitas_10,
                               'target_satuan_waktu'     : 'hari',
                               'target_angka_kredit'     : template_obj.target_angka_kredit_10,
                               'target_biaya'     : template_obj.target_biaya_10,
                     })
            task_id = task_pool.create(cr, user_id, task,context)

        if template_obj.active_11:
            satuan_hitung_id,satuan_hitung_msg = self.get_satuan_satuan_hitung(cr,uid,template_obj.target_satuan_kuantitas_output_11)
            task.update({
                               'target_period_month':'11',
                               'target_jumlah_kuantitas_output'     : template_obj.target_jumlah_kuantitas_output_11,
                               'target_satuan_kuantitas_output'     : satuan_hitung_id,
                               'target_waktu'     : template_obj.target_waktu_11,
                               'target_kualitas'     : template_obj.target_kualitas_11,
                               'target_satuan_waktu'     : 'hari',
                               'target_angka_kredit'     : template_obj.target_angka_kredit_11,
                               'target_biaya'     : template_obj.target_biaya_11,
                     })
            task_id = task_pool.create(cr, user_id, task,context)

        if template_obj.active_12:
            satuan_hitung_id,satuan_hitung_msg = self.get_satuan_satuan_hitung(cr,uid,template_obj.target_satuan_kuantitas_output_12)
            task.update({
                               'target_period_month':'12',
                               'target_jumlah_kuantitas_output'     : template_obj.target_jumlah_kuantitas_output_12,
                               'target_satuan_kuantitas_output'     : satuan_hitung_id,
                               'target_waktu'     : template_obj.target_waktu_12,
                               'target_kualitas'     : template_obj.target_kualitas_12,
                               'target_satuan_waktu'     : 'hari',
                               'target_angka_kredit'     : template_obj.target_angka_kredit_12,
                               'target_biaya'     : template_obj.target_biaya_12,
                     })
            task_id = task_pool.create(cr, user_id, task,context)
        return True

    def get_company_by_code(self, cr, uid, company_code):
        company_pool = self.pool.get('res.company')
        company_ids = company_pool.search(cr, uid, [('code','=',company_code)], context=None)
        if not company_ids:
            return None,'Kode SKPD tidak ada yang sesuai'
        elif len(company_ids) > 1 :
            return None,'Terdapat duplikasi kode SKPD'
        elif len(company_ids) == 1 :
            company_obj = company_pool.browse(cr, uid, company_ids, context=None)[0]
            return company_obj,'Sukses'
        
        return None.None
    def get_company_by_name(self, cr, uid, company_name):
        company_pool = self.pool.get('res.company')
        company_ids = company_pool.search(cr, uid, [('name','=',company_name)], context=None)
        if not company_ids:
            return None,'SKPD Tidak ada dalam daftar'
        elif len(company_ids) > 1 :
            return None,'Terdapat duplikasi kode SKPD'
        elif len(company_ids) == 1 :
            company_obj = company_pool.browse(cr, uid, company_ids, context=None)[0]
            return company_obj,'Sukses'
        
        return None.None
    def get_employee_by_nip(self, cr, uid, nip):
        employee_pool = self.pool.get('res.partner')
        employee_ids = employee_pool.search(cr, uid, [('nip','=',nip),('employee','=',True)], context=None)
        if not employee_ids:
            return None,'Tidak NIP ada yang sesuai'
        elif len(employee_ids) > 1 :
            return None,'Terdapat duplikasi NIP di data kepegawaian'
        elif len(employee_ids) == 1 :
            employee_obj = employee_pool.browse(cr, uid, employee_ids, context=None)[0]
            return employee_obj,'Sukses'

        return None,None

    def get_satuan_satuan_hitung(self, cr, uid, satuan_hitung_name):
        satuan_hitung_pool = self.pool.get('satuan.hitung')
        satuan_hitung_ids = satuan_hitung_pool.search(cr, uid, [('name','=',satuan_hitung_name),('active','=',True)], context=None)
        if not satuan_hitung_ids:
            new_satuan_task_id = satuan_hitung_pool.create(cr, SUPERUSER_ID, {'name':satuan_hitung_name,'active':True,},context=None)
            return new_satuan_task_id,'Sukses'
        elif len(satuan_hitung_ids) > 1 :
            satuan_hitung_obj = satuan_hitung_pool.browse(cr, uid, satuan_hitung_ids, context=None)[0]
            return satuan_hitung_obj.id,'Sukses'
        elif len(satuan_hitung_ids) == 1 :
            satuan_hitung_obj = satuan_hitung_pool.browse(cr, uid, satuan_hitung_ids, context=None)[0]
            return satuan_hitung_obj.id,'Sukses'

        return None,None
    
  
project_template()
