##############################################################################
#
#    Darmawan Fatriananda
#    Maxidio
#    Copyright (c) 2015 <http://www.maxidio.co>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

{
    "name": "SKP By Admin OPD",
    "version": "1.0",
    "author": "Darmawan Fatriananda",
    "category": "project",
    "description": """
  
    """,
    "website" : "http://www.-",
    "license" : "GPL-3",
    "depends": [
                "df_project",
                ],
    'data': [      "company_view.xml",
                       "project_template_view.xml",
                       "project_monthly_template_view.xml",
                       "project_perilaku_monthly_template_view.xml",
              ],
    
}
