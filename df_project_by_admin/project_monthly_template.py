# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Kab Belitung Timur
#    Copyright (c) 2015 <http://www.->
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp
from openerp import SUPERUSER_ID
import time
from mx import DateTime



class project_monthly_template(osv.Model):
    _name = 'project.monthly.template'
    _description='Template Project Bulanan By admin'
    _columns = {
        'company_name'         : fields.char('SKPD',size=64, required=True),
        'company_code'          : fields.char('Kode OPD',size=18, required=False),
        'target_period_year'    : fields.char('Periode Tahun',size=4, required=True),
        'target_period_month'     : fields.selection([('01', 'Januari'), ('02', 'Februari'),
                                                      ('03', 'Maret'), ('04', 'April'),
                                                      ('05', 'Mei'), ('06', 'Juni'),
                                                      ('07', 'Juli'), ('08', 'Agustus'),
                                                      ('09', 'September'), ('10', 'Oktober'),
                                                      ('11', 'November'), ('12', 'Desember')], 'Periode Bulan', required=True
                                                     ),

        'name'                       : fields.char('Nama Kegiatan',size=500, required=True),
        'code'                        : fields.char('Kode Kegiatan',size=25,required=False),

        'nip'                           : fields.char('NIP',size=20, required=True),
        'employee_name'        : fields.char('Nama Pegawai',size=64, required=False),
        'target_type_id': fields.selection([          ('dpa_opd_biro', 'Kegiatan APBD - DPA'),
                                                      ('dipa_apbn', 'Kegiatan APBN - DIPA'),  
                                                      ('sotk', 'Kegiatan Tupoksi (Non DPA)'), 
                                                      ('lain_lain', 'Kegiatan Target Pendapatan Daerah'), 
                                                      ],
                                                      'Jenis Kegiatan', required=False, 
                                                     ),
         'target_category_id': fields.selection([     ('program', 'Program'), 
                                                      ('kegiatan', 'Kegiatan'), 
                                                      ],
                                                      'Kategori',
                                                     ),
        'realisasi_jumlah_kuantitas_output'     : fields.float('Kuantitas Output',required=True,digits_compute=dp.get_precision('no_digit')),
        'realisasi_satuan_kuantitas_output'         : fields.char('Satuan Kuantitas Output',size=64, required=True),
        'realisasi_kualitas'     : fields.float('Kualitas',required=True,digits_compute=dp.get_precision('no_digit')),
        'realisasi_waktu'     : fields.integer('Waktu',required=True,digits_compute=dp.get_precision('no_digit')),
        'realisasi_biaya'     : fields.float('Biaya',),
        'realisasi_angka_kredit'                       : fields.float('Angka Kredit' ,digits_compute=dp.get_precision('angka_kredit')),

        'task_id': fields.many2one('project.skp', 'Realisasi Bulanan', ondelete='set null', ),
        'state': fields.selection([     ('new', 'Baru'),  ('valid', 'Valid'),  ('invalid', 'Invalid'), ('done', 'Selesai'),   ],
                                                      'Status',readonly=True
                                                     ),
          'error_count'     : fields.integer('Jumlah Revisi',readonly=True),
          'notes'     : fields.text('Catatan'),
          'user_id_admin_opd': fields.many2one('res.users', 'Petugas Admin Dari SKPD', 
            help='Staff admin SKPD'),
          'user_id': fields.many2one('res.users', 'Pegawai Yang Dinilai',),
          'user_id_atasan': fields.many2one('res.users', 'Atasan Langsung', ),
          'user_id_banding': fields.many2one('res.users', 'Atasan Banding', ),
    }
    _defaults = {
        'state':'new',
    }

    def action_realisasi_evaluated(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        task_pool = self.pool.get('project.skp')
        for template_obj in self.browse(cr, uid, ids, context=context):
            task_pool.action_propose(cr,template_obj.user_id.id,[template_obj.task_id.id],context=None)
            task_pool.action_evaluated(cr,template_obj.user_id_atasan.id,[template_obj.task_id.id],context=None)
            self.write(cr,uid,[template_obj.id],{'state':'done',},context=None)

        return True


    def create_realisasi_from_template(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        task_pool = self.pool.get('project.skp')
        target_template_pool = self.pool.get('project.template')
       
        task =  {}
        now=DateTime.today();
       

        for template_obj in self.browse(cr, uid, ids, context=context):
              vals = {}
              error_count = 0
              notes='-'
              task_ids=None
              company_obj , company_err_msg= target_template_pool.get_company_by_name(cr,uid,template_obj.company_name)
              employee_obj, employee_err_msg = target_template_pool.get_employee_by_nip(cr,uid,template_obj.nip)

              
              if not employee_obj:
                    error_count+=1
                    notes= notes+' '+employee_err_msg+', '
              else :
                  task_ids = task_pool.search(cr, uid, [('target_period_year','=',template_obj.target_period_year),
                                                                    ('target_period_month','=',template_obj.target_period_month),
                                                                     ('user_id','=',employee_obj.user_id.id),
                                                                    ('name','=',template_obj.name),
                                                                    ('active','=',True)], context=None)
              if not company_obj:
                    error_count+=1
                    notes= notes+' '+company_err_msg+', '
              if not task_ids and employee_obj:
                    error_count+=1
                    notes= notes+' '+' Kegiatan Tidak Ditemukan '+', '
              if error_count > 0 :
                    self.write(cr,uid,[template_obj.id],{   'state':'invalid','error_count' :error_count, 'notes':notes,
                                                                },context=None)
                    continue;

              
              if task_ids :
                satuan_hitung_id,satuan_hitung_msg = target_template_pool.get_satuan_satuan_hitung(cr,uid,template_obj.realisasi_satuan_kuantitas_output)

                vals.update({
                                      'realisasi_jumlah_kuantitas_output'     : template_obj.realisasi_jumlah_kuantitas_output,
                                      'realisasi_satuan_kuantitas_output'     : satuan_hitung_id,
                                      'realisasi_angka_kredit'                     : template_obj.realisasi_angka_kredit,
                                      'realisasi_kualitas'                    : template_obj.realisasi_kualitas,
                                      'realisasi_waktu'                       : template_obj.realisasi_waktu,
                                      'realisasi_satuan_waktu'                : 'hari',
                                      'realisasi_biaya'                       : template_obj.realisasi_biaya,
                                  })

                task_pool.write(cr, employee_obj.user_id.id, task_ids, vals, context=None)
                for task_id in task_ids:
                    self.write(cr,uid,template_obj.id,{'task_id'     : task_id,'state':'valid',
                                                            'user_id':employee_obj.user_id.id,
                                                             'user_id_atasan':employee_obj.user_id_atasan.user_id.id,
                                                             'user_id_banding':employee_obj.user_id_banding.user_id.id,
                                                             'error_count' :error_count,
                                                              'notes':notes,
                                                              'user_id_admin_opd' : company_obj.user_id_admin_opd.id,
                      },context=None)

      
      

project_monthly_template()

class project_perilaku_monthly_template(osv.Model):
    _name = 'project.perilaku.monthly.template'
    _description='Template Project Perilaku Bulanan By admin'
    _columns = {
        'company_name'         : fields.char('SKPD',size=64, required=True),
        'company_code'          : fields.char('Kode OPD',size=18, required=False),
        'target_period_year'    : fields.char('Periode Tahun',size=4, required=True),
        'target_period_month'     : fields.selection([('01', 'Januari'), ('02', 'Februari'),
                                                      ('03', 'Maret'), ('04', 'April'),
                                                      ('05', 'Mei'), ('06', 'Juni'),
                                                      ('07', 'Juli'), ('08', 'Agustus'),
                                                      ('09', 'September'), ('10', 'Oktober'),
                                                      ('11', 'November'), ('12', 'Desember')], 'Periode Bulan', required=True
                                                     ),
         'name'          : fields.char('Nama',size=25, required=False),
        'nip'                           : fields.char('NIP',size=20, required=True),
        'employee_name'        : fields.char('Nama Pegawai',size=64, required=False),

        'perilaku_id': fields.many2one('project.perilaku', 'Realisasi Bulanan', ondelete='set null', ),
        'state': fields.selection([     ('new', 'Baru'),  ('valid', 'Valid'),  ('invalid', 'Invalid'), ('done', 'Selesai'),   ],
                                                      'Status',readonly=True
                                                     ),
         'error_count'     : fields.integer('Jumlah Revisi',readonly=True),
         'notes'     : fields.text('Catatan'),
          'user_id_admin_opd': fields.many2one('res.users', 'Petugas Admin Dari SKPD',   help='Staff admin SKPD'),
          'user_id': fields.many2one('res.users', 'Pegawai Yang Dinilai',),
          'user_id_atasan': fields.many2one('res.users', 'Atasan Langsung', ),
          'user_id_banding': fields.many2one('res.users', 'Atasan Banding', ),


          #poin Perilaku 
        'realisasi_jumlah_konsumen_pelayanan'     : fields.integer('Jumlah Pelayanan'),
        'realisasi_jumlah_tidakpuas_pelayanan'     : fields.integer('Jumlah Ketidakpuasan Pelayanan'),
        'realisasi_satuan_jumlah_konsumen_pelayanan': fields.char('Satuan Nilai Konsumen',size=120),
        'realisasi_ketepatan_laporan_spj'     : fields.char('Ketepatan Laporan SPJ', size=25),
        'realisasi_ketepatan_laporan_ukp4'     : fields.char('Ketepatan Laporan UKP4', size=25 ), 
        'realisasi_efisiensi_biaya_operasional'     : fields.char('Efisiensi Biaya Operasional Kantor', size=25),
        'realisasi_hadir_apel_pagi'     : fields.integer('Kehadiran Apel Pagi'),
        'realisasi_hadir_hari_kerja'     : fields.integer('Kehadiran Hari Kerja'),
        'realisasi_hadir_jam_kerja': fields.integer('Kehadiran Jam Kerja'),
        'realisasi_hadir_upacara_hari_besar': fields.integer('Kehadiran Upacara Hari Besar'),
        'realisasi_integritas_presiden': fields.boolean('Presiden'),
        'realisasi_integritas_gubernur': fields.boolean('Gubernur'),
        'realisasi_integritas_kepalaopd': fields.boolean('Kepala OPD'),
        'realisasi_integritas_atasan': fields.boolean('Atasan Langsung'),
        'realisasi_integritas_lainlain': fields.boolean('Lain Lain'),
        'realisasi_integritas_es1': fields.boolean('Pejabat Eselon I'),
        'realisasi_integritas_es2': fields.boolean('Pejabat Eselon II'),
        'realisasi_integritas_es3': fields.boolean('Pejabat Eselon III'),
        'realisasi_integritas_es4': fields.boolean('Pejabat Eselon IV'),
        'realisasi_integritas_hukuman': fields.char('Ada Hukuman Disiplin',size=5),
        'realisasi_integritas_hukuman_value': fields.char('Jenis Hukuman Disiplin',size=15),
        'realisasi_integritas_hukuman_ringan': fields.boolean('Ringan'),
        'realisasi_integritas_hukuman_sedang': fields.boolean('Sedang'),
        'realisasi_integritas_hukuman_berat': fields.boolean('Berat'),
        
        'realisasi_kerjasama_nasional': fields.boolean('Nasional'),
        'realisasi_kerjasama_gubernur': fields.boolean('Provinsi'),
        'realisasi_kerjasama_kepalaopd': fields.boolean('OPD'),
        'realisasi_kerjasama_atasan': fields.boolean('Atasan Langsung'),
        'realisasi_kerjasama_lainlain': fields.boolean('Lain Lain'),
        'realisasi_kerjasama_rapat_atasan': fields.boolean('Rapat Atasan Langsung'),
        'realisasi_kerjasama_rapat_perangkat_daerah': fields.boolean('Rapat Perangkat Daerah'),
        'realisasi_kerjasama_rapat_provinsi': fields.boolean('Rapat Provinsi'),
        'realisasi_kerjasama_rapat_nasional': fields.boolean('Rapat Nasional'),
        'realisasi_kepemimpinan_nasional': fields.boolean('Nasional'),
        'realisasi_kepemimpinan_gubernur': fields.boolean('Provinsi'),
        'realisasi_kepemimpinan_kepalaopd': fields.boolean('OPD'),
        'realisasi_kepemimpinan_atasan': fields.boolean('Atasan Langsung'),
        'realisasi_kepemimpinan_lainlain': fields.boolean('Lain Lain'),
        'realisasi_kepemimpinan_narsum_unitkerja': fields.boolean('Narasumber Kerja Atasan Langsung'),
        'realisasi_kepemimpinan_narsum_perangkat_daerah': fields.boolean('Narasumber Perangkat Daerah'),
        'realisasi_kepemimpinan_narsum_provinsi': fields.boolean('Narasumber Provinsi'),
        'realisasi_kepemimpinan_narsum_nasional': fields.boolean('Narasumber Nasional'),

        #attachment    
        'realisasi_integritas_presiden_attach': fields.char('No Bukti',size=50,placeholder='No SK/Bukti....'),
        'realisasi_integritas_gubernur_attach': fields.char('No Bukti',size=50,placeholder='No SK/Bukti....'),
        'realisasi_integritas_kepalaopd_attach': fields.char('No Bukti',size=50,placeholder='No SK/Bukti....'),
        'realisasi_integritas_es1_attach': fields.char('No Bukti',size=50,placeholder='No SK/Bukti....'),
        'realisasi_integritas_es2_attach': fields.char('No Bukti',size=50,placeholder='No SK/Bukti....'),
        
        'realisasi_kerjasama_nasional_attach': fields.char('No Bukti',size=50,placeholder='No SK/Bukti....'),
        'realisasi_kerjasama_gubernur_attach': fields.char('No Bukti',size=50,placeholder='No SK/Bukti....'),
        'realisasi_kerjasama_kepalaopd_attach': fields.char('No Bukti',size=50,placeholder='No SK/Bukti....'),
        'realisasi_kerjasama_lainlain_attach': fields.char('No Bukti',size=50,placeholder='No SK/Bukti....'),
        'realisasi_kerjasama_rapat_provinsi_attach': fields.char('No Bukti',size=50,placeholder='No SK/Bukti....'),
        'realisasi_kerjasama_rapat_nasional_attach': fields.char('No Bukti',size=50,placeholder='No SK/Bukti....'),
        
        'realisasi_kepemimpinan_nasional_attach': fields.char('No Bukti',size=50,placeholder='No SK/Bukti....'),
        'realisasi_kepemimpinan_gubernur_attach': fields.char('No Bukti',size=50,placeholder='No SK/Bukti....'),
        'realisasi_kepemimpinan_kepalaopd_attach': fields.char('No Bukti',size=50,placeholder='No SK/Bukti....'),
        'realisasi_kepemimpinan_lainlain_attach': fields.char('No Bukti',size=50,placeholder='No SK/Bukti....'),
        'realisasi_kepemimpinan_narsum_unitkerja_attach': fields.char('No Bukti',size=50,placeholder='No SK/Bukti....'),
        'realisasi_kepemimpinan_narsum_provinsi_attach': fields.char('No Bukti',size=50,placeholder='No SK/Bukti....'),
        'realisasi_kepemimpinan_narsum_nasional_attach': fields.char('No Bukti',size=50,placeholder='No SK/Bukti....'),
        
        #date
        'realisasi_integritas_presiden_date' :fields.date('Tanggal SK/Bukti'),
        'realisasi_integritas_gubernur_date' :fields.date('Tanggal SK/Bukti'),
        'realisasi_integritas_kepalaopd_date' :fields.date('Tanggal SK/Bukti'),
        'realisasi_integritas_es1_date' :fields.date('Tanggal SK/Bukti'),
        'realisasi_integritas_es2_date' :fields.date('Tanggal SK/Bukti'),
        
        'realisasi_kerjasama_nasional_date' :fields.date('Tanggal SK/Bukti'),
        'realisasi_kerjasama_gubernur_date' :fields.date('Tanggal SK/Bukti'),
        'realisasi_kerjasama_kepalaopd_date' :fields.date('Tanggal SK/Bukti'),
        'realisasi_kerjasama_lainlain_date' :fields.date('Tanggal SK/Bukti'),
        'realisasi_kerjasama_rapat_provinsi_date' :fields.date('Tanggal SK/Bukti'),
        'realisasi_kerjasama_rapat_nasional_date' :fields.date('Tanggal SK/Bukti'),
        
        'realisasi_kepemimpinan_nasional_date' :fields.date('Tanggal SK/Bukti'),
        'realisasi_kepemimpinan_gubernur_date' :fields.date('Tanggal SK/Bukti'),
        'realisasi_kepemimpinan_kepalaopd_date' :fields.date('Tanggal SK/Bukti'),
        'realisasi_kepemimpinan_lainlain_date' :fields.date('Tanggal SK/Bukti'),
        'realisasi_kepemimpinan_narsum_unitkerja_date' :fields.date('Tanggal SK/Bukti'),
        'realisasi_kepemimpinan_narsum_provinsi_date' :fields.date('Tanggal SK/Bukti'),
        'realisasi_kepemimpinan_narsum_nasional_date' :fields.date('Tanggal SK/Bukti'),

        }

project_perilaku_monthly_template()

class project_tambahan_kreatifitas_monthly_template(osv.Model):
    _name = 'project.tambahan.kreatifitas.monthly.template'
    _description='Template Project Tambahan Dan Kreatifitas Bulanan By admin'
    _columns = {
        'company_name'         : fields.char('SKPD',size=64, required=True),
        'company_code'          : fields.char('Kode OPD',size=18, required=False),
        'target_period_year'    : fields.char('Periode Tahun',size=4, required=True),
        'target_period_month'     : fields.selection([('01', 'Januari'), ('02', 'Februari'),
                                                      ('03', 'Maret'), ('04', 'April'),
                                                      ('05', 'Mei'), ('06', 'Juni'),
                                                      ('07', 'Juli'), ('08', 'Agustus'),
                                                      ('09', 'September'), ('10', 'Oktober'),
                                                      ('11', 'November'), ('12', 'Desember')], 'Periode Bulan', required=True
                                                     ),
         'name'          : fields.char('Nama',size=25, required=False),
        'nip'                           : fields.char('NIP',size=20, required=True),
        'employee_name'        : fields.char('Nama Pegawai',size=64, required=False),

        'tambahan_kreatifitas_id': fields.many2one('project.tambahan.kreatifitas', 'Realisasi Bulanan', ondelete='set null', ),
        'state': fields.selection([     ('new', 'Baru'),  ('valid', 'Valid'),  ('invalid', 'Invalid'), ('done', 'Selesai'),   ],
                                                      'Status',readonly=True
                                                     ),
         'error_count'     : fields.integer('Jumlah Revisi',readonly=True),
         'notes'     : fields.text('Catatan'),
          'user_id_admin_opd': fields.many2one('res.users', 'Petugas Admin Dari SKPD',   help='Staff admin SKPD'),
          'user_id': fields.many2one('res.users', 'Pegawai Yang Dinilai',),
          'user_id_atasan': fields.many2one('res.users', 'Atasan Langsung', ),
          'user_id_banding': fields.many2one('res.users', 'Atasan Banding', ),

          'realisasi_tugas_tambahan'     : fields.integer('Jumlah Tugas Tambahan'),
          'realisasi_uraian_tugas_tambahan'     : fields.text('Uraian Tugas Tambahan'),
           'realisasi_rl_tugas_tambahan'     : fields.char('Ruang Lingkup Tugas Tambahan',size=25),
           'realisasi_nilai_kreatifitas'     : fields.integer('Jumlah Kreatifitas'),
           'realisasi_uraian_kreatifitas'     : fields.text('Uraian Kreatifitas'),
            'realisasi_tupoksi_kreatifitas': fields.boolean('Tupoksi'),
             'realisasi_rl_kreatifitas'     : fields.boolean('Ruang Lingkup Kreatifitas',size=25),
    }
project_tambahan_kreatifitas_monthly_template()