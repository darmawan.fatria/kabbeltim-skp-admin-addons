
# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Kab Belitung Timur
#    Copyright (c) 2015 <http://www.->
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
from openerp import SUPERUSER_ID

class project_skp(osv.Model):
    _inherit = 'project.skp'
    _columns = {
        'user_id_admin_opd': fields.many2one('res.users', 'Petugas Admin Dari SKPD', 
            help='Staff admin SKPD'),
    }
   
project_skp()
