from openerp.osv import fields, osv
from string import split

#class notification_user_activation_done(osv.osv_memory):
 #   _name = "notification.user.activation.done"
  #  _columns = {
   #     'name': fields.char('Notif', size=128),
   # }
#notification_user_activation_done();
class app_skp_automation_configuration(osv.osv_memory):
    _name = 'app.skp.automation.configuration'
    _description = 'Konfigurasi Proses Automisasi SKP'

    _columns = {
            'name' : fields.char('Konfigurasi Proses Template', size=64, readonly=True),
            'param_type'     : fields.selection([('all', 'Semua'), ('skp', 'SKP'),
                                                      ('perilaku', 'Perilaku'), ('tk', 'Tambahan Dan Kreatifitas')], 'Jenis Kegiatan'
                                                     ),
            'param_target_period_month'     : fields.selection([('01', 'Januari'), ('02', 'Februari'),
                                                      ('03', 'Maret'), ('04', 'April'),
                                                      ('05', 'Mei'), ('06', 'Juni'),
                                                      ('07', 'Juli'), ('08', 'Agustus'),
                                                      ('09', 'September'), ('10', 'Oktober'),
                                                      ('11', 'November'), ('12', 'Desember')], 'Periode Bulan'
                                                     ),
            'param_target_period_year'     : fields.char('Periode Tahun', size=4, ),
         
    }
    def validate_all_target_template(self, cr, uid, ids, context={}):

        project_template_pool = self.pool.get('project.template')
        for param_obj in self.browse(cr, uid, ids, context=context):
                param_obj.param_target_period_year
                project_template_ids = project_template_pool.search(cr, uid, [('target_period_year','=',param_obj.param_target_period_year),
                                                                     ('create_uid','=',uid),
                                                                    ('state','=','new'),
                                                                    ], context=None)
                for template_id in  project_template_ids:
                        try :
                            project_template_pool.create_target_from_template(cr,uid,[template_id],context=None)
                        except :
                            print "Project Template error : ",template_id
        
        return {}

    def do_verification_all_target_template(self, cr, uid, ids, context={}):

        project_template_pool = self.pool.get('project.template')
        for param_obj in self.browse(cr, uid, ids, context=context):
                param_obj.param_target_period_year
                project_template_ids = project_template_pool.search(cr, uid, [('target_period_year','=',param_obj.param_target_period_year),
                                                                     ('create_uid','=',uid),
                                                                    ('state','=','valid'),
                                                                    ], context=None)
                for template_id in  project_template_ids:
                        try :
                            project_template_pool.action_target_evaluated(cr,uid,[template_id],context=None)
                        except :
                            print "Project Template error : ",template_id
        
        return {}

    def validate_all_realisasi_template(self, cr, uid, ids, context={}):

        project_montly_template_pool = self.pool.get('project.monthly.template')
        for param_obj in self.browse(cr, uid, ids, context=context):
                data_ids = project_montly_template_pool.search(cr, uid, [('target_period_year','=',param_obj.param_target_period_year),
                    ('target_period_month','=',param_obj.param_target_period_month),
                                                                     ('create_uid','=',uid),
                                                                    ('state','=','new'),
                                                                    ], context=None)
                for task_id in  data_ids:
                        try :
                            project_montly_template_pool.create_realisasi_from_template(cr,uid,[task_id],context=None)
                        except :
                            print "Project Template error : ",task_id
        
        return {}
    def do_verification_all_realisasi_template(self, cr, uid, ids, context={}):

        project_montly_template_pool = self.pool.get('project.monthly.template')
        for param_obj in self.browse(cr, uid, ids, context=context):
                data_ids = project_montly_template_pool.search(cr, uid, [('target_period_year','=',param_obj.param_target_period_year),
                    ('target_period_month','=',param_obj.param_target_period_month),
                                                                     ('create_uid','=',uid),
                                                                    ('state','=','valid'),
                                                                    ], context=None)
                for task_id in  data_ids:
                        try :
                            project_montly_template_pool.action_realisasi_evaluated(cr,uid,[task_id],context=None)
                        except :
                            print "Project Template error : ",task_id
        
        return {}
    
    def deactivate_all_users_opd(self, cr, uid,ids, context=None):
        
        for param_obj in self.browse(cr, uid, ids, context=context):
            if param_obj.param_company_id:
                company_id = param_obj.param_company_id.id
                cr.execute('update res_users u set active=%s from res_partner p where p.user_id = u.id and u.company_id=%s and p.employee ', (False,company_id))
        
        return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'notification.user.activation.done',
                'target': 'new',
                'context': context,
            }
app_skp_automation_configuration()